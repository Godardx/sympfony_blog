# /!\ Ceci sert de compte-rendu /!\

## Comment faire fonctionner le projet !

Lien du Git : https://gitlab.com/Godardx/sympfony_blog

---

- Faites un `git clone` du projet et ensuite lancer
- Il vous faudra créer la base de données de `blog_thomas` avec la commande `php bin/console doctrine:database:create ` 
- Pour lancer le serveur il faudra faire un `symfony:start`
- Pour avoir les users de créer et les 20 articles de bases il faudra faire un `php bin/console doctrine:fixtures:load -n`

---

 ### Liste des comptes pour pouvoir tester les fonctionnalitées

- Admin :
    - Mail : admin@ex.com
    - Mot de passe : admin
- User :
    - Mail : user@ex.com
    - Mot de passe : user

---

### Choix graphique

Le choix graphique à été fait avec une librairie classique qui rajoute de la couleur au pages et à la navbar pour plus d'esthétisme.

---

### Difficultés rencontrées

- Le manque de motivation
- La compréhension du langage
- Le manque de temps du à une mauvaise organisation
