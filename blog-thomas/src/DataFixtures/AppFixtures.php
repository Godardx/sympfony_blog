<?php

namespace App\DataFixtures;

use App\Entity\Articles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 21; $i++) {
            $article = new Articles();
            $article
                ->setTitle('Ceci est un titre')
                ->setDescription('Ici il y à une description');


            // $product = new Product();
            $manager->persist($article);
        }

        $manager->flush();
    }
}
